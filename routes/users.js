const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const { QueryTypes } = require('sequelize');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const model = require('../models/index');
const passportJWT = require('../middlewares/passport-jwt');

// console.log(process.env.PORT)
router.get('/profile', [passportJWT.isLogin] , async function(req, res, next) {
  const user = await model.User.findByPk(req.user.user_id);

  return res.status(200).json({
    user: {
      id: user.id,
      fullname: user.fullname,
      email: user.email,
      created_at: user.created_at
    }
  });
});

/* GET users listing. */
router.get('/', async function(req, res, next) {
  // res.send('respond with a resource');
  const users = await model.User.findAll({
    attributes:{exclude:['password']},
    order:[['id','desc']]
  });
  const sql = "SELECT * FROM Users"
  const users2 = await model.sequelize.query(sql, { type: QueryTypes.SELECT });
  const totalUser = await model.User.count();
  return res.status(200).json({
    total: totalUser,
    data: users2
  });
});
router.post('/register',async function(req,res,next){
  const {fullname,email,password} = req.body;
  const user = await model.User.findOne({ where: { email: email } });
  if (user !== null) {
    return res.status(400).json({message: 'มีผู้ใช้งานอีเมล์นี้แล้ว',data:email});
  }

  const passwordHash=await argon2.hash(password);
  const newUser = await model.User.create({
    fullname: fullname,
    email: email,
    password: passwordHash
  });

  return res.status(201).json({
    data:req.body,
    message:"ลงทะเบียนสำเร็จ",
    passhash:passwordHash
  });

});

router.post('/login',async function(req,res,next){
  const {email,password} = req.body;
  const user = await model.User.findOne({ where: { email: email } });
  if (user === null) {
    return res.status(404).json({message: 'ไม่มีผู้ใช้งานอีเมล์นี้'});
  }

  const isvalid=await argon2.verify(user.password,password);
  if (!isvalid) {
    return res.status(401).json({message: 'รหัสผ่านไม่ถูกต้อง'});
  }
  const token= jwt.sign({user_id:user.id},process.env.JWT_KEY,{expiresIn:"7d"})
  return res.status(201).json({
    message:"เข้าระบบสำเร็จ123",
    access_token: token,
    data: req.body
  });

});
// Yot 2
module.exports = router;
